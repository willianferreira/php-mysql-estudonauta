<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Listagem de Jogos</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="estilos/estilo.css"/>
</head>
<body>
	<?php 
		// include = se não encontra o banco continua execução
		// require = se der algum erro ele para execução
		// require_onde = caso importar duas vezes, ele vai importar uma única
		require_once 'includes/banco.php';
		require_once 'includes/funcoes.php';
	?>
	<div id="corpo">
		<h1>Escolha seu jogo</h1>
		<table class="listagem">
		<?php

            
			if (!$busca) {
				echo "<tr><td>Infelizmente a busca deu errado";
			} else {
				if ($busca->num_rows == 0) {
					echo "<tr><td>Nenhum registro encontrado";
				} else {
					while($reg = $busca->fetch_object()) {
						$t = thumb($reg->capa) ?>
						<tr>
                            <td>
                                <?php echo "<img src='$t' class='mini'/>"; ?>
                            </td>
						    <td>
                            <?php echo "<a href='detalhes.php?cod=$reg->cod'>$reg->nome</a>"; ?>
						    </td>
						    <td>Adm</td>
                        </tr>
				<?php
                	}
				}
			}
			?>
		</table>
	</div>
	<?php $banco->close();?>
</body>
</html>